/********************************************************************
*
*	CLASS		:: Level
*	DESCRIPTION	:: Loads in a level from a file.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 03 / 13
*
********************************************************************/

#ifndef LevelH
#define LevelH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBERenderable.h>
#include <DBETypes.h>

#include "Building.h"
#include "LevelShader.h"

namespace DBE {
	class Vec3;
}
struct ID3D11Buffer;
struct Texture;
struct VertPos3fColour4ubNormal3fTex2f;
/********************************************************************
*	Defines and constants.
********************************************************************/
enum TexelCode {
	TC_Invalid = 0,
	TC_TownHall,	// Red
	TC_Road,		// Blue
	TC_Grass,		// Green
	TC_Plot,		// Black
	TC_Count,
};


/*******************************************************************/
class Level : public DBE::Renderable {
	public:
		/// Constructor.
		Level();
		/// Destructor.
		~Level();
		
		/// Updates the level.
		bool OnUpdate( float deltaTime);
		/// Renders the level.
		void OnRender( float deltaTime);

		/// Loads a level.
		bool LoadLevel( s32 l);

		void AddGold( u32 g);
		void LoseGold( u32 g);
		u32 GetGold() const;

		void PickTile();
		void DeselectTile();

		void SelectBuilding( u32 option);

		s32 GetHoveredTile() const;
		float GetHoveredTileHightlight() const;
		

	private:
		struct TileData {
			// Type of tile.
			TexelCode m_code;

			// For plots.
			s32		m_buildingOption;
			float	m_buildingTime;
			bool	m_built;
			float	m_yeildTime;

			// For rendering.
			DBE::Vec3	m_offset;
			u32			m_colour;
			float		m_highlight;
		};

		//void RenderTile();

		void CreatePlane( float width = 1.0f, float height = 1.0f, float depth = 1.0f, u32 colour = 0);
		TexelCode DecipherTexel( u8 r, u8 g, u8 b);

		bool TestForPickedTile();

		Resources m_resources;

		TileData** mp_data;
		
		TileData* mp_hovered;
		TileData* mp_picked;

		float m_highlightMin;
		float m_highlightMax;
		float m_highlightInc;
		float m_highlightSelected;

		Texture* mp_invalid;
		Texture* mp_grass;
		Texture* mp_road;
		Texture* mp_plot;

		LevelVertexShader*	mp_tileVS;
		LevelPixelShader*	mp_tilePS;

		ID3D11Buffer* mp_vertexBuffer;
		ID3D11Buffer* mp_indexBuffer;

		VertPos3fColour4ubNormal3fTex2f*	mp_verts;
		UINT*								mp_indicies;

		UINT m_indexCount;

		
		/// Private copy constructor to prevent accidental multiple instances.
		Level( const Level& other);
		/// Private assignment operator to prevent accidental multiple instances.
		Level& operator=( const Level& other);
		
};

/*******************************************************************/
#endif	// #ifndef LevelH
