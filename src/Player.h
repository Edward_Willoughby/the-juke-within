/********************************************************************
*
*	CLASS		:: Player
*	DESCRIPTION	:: Holds the data pertaining to the player.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 06 / 06
*
********************************************************************/

#ifndef PlayerH
#define PlayerH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEBasicMesh.h>

/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class Player : public DBE::BasicMesh {
	public:
		/// Constructor.
		Player();
		/// Destructor.
		~Player();
		
		bool Update( float deltaTime);
		void Render( float deltaTime);

		s32 GetHealth();
		void Heal( s32 gainedHealth);
		void Injure( s32 lostHealth);

		bool HasFlash();
		void Flash( const DBE::Vec2& mousePos, const DBE::BasicMesh* p_floor);
		
	private:
		s32 m_health;

		float m_flashTimer;

		DBE::BasicMesh* mp_testBall;
		
		/// Private copy constructor to prevent accidental multiple instances.
		Player( const Player& other);
		/// Private assignment operator to prevent accidental multiple instances.
		Player& operator=( const Player& other);
		
};

/*******************************************************************/
#endif	// #ifndef PlayerH
