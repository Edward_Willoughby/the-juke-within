/********************************************************************
*	Function definitions for the UserInterface class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "UserInterface.h"

#include <DBEApp.h>
#include <DBETypes.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;



/**
* Constructor.
*/
UserInterface::UserInterface() {

}

/**
* Destructor.
*/
UserInterface::~UserInterface() {

}

/**
* 
*
* @param :: 
*
* @return 
*/
bool UserInterface::Init() {
	mp_options = nullptr;
	m_optionCount = 0;
	m_optionHovered = -1;

	ID3D11SamplerState* sampler( GET_APP()->GetSamplerState( true, true, true));

	//char buffer[256];
	m_optionsBuildings = new UIOption[Building::GetBuildingCount()];
	for( u32 i( 0); i < Building::GetBuildingCount(); ++i) {
		m_optionsBuildings[i].m_pos = Vec3( (0.12f * i) + 0.05f, -(1.0f / 5) * 2, 1.0f);
		m_optionsBuildings[i].m_scale = Vec3( 0.1f, 0.1f, 1.0f);

		m_optionsBuildings[i].mp_building = Building::GetBuilding( i);
		//sprintf_s( buffer, 256, "%s%s.dds", "res/textures/", gsc_optionsBuildingsTextures[i]);
		//m_optionsBuildings[i].mp_texture = MGR_TEXTURE().LoadTexture( buffer, sampler);
	}

	mp_textureOptionBorder = MGR_TEXTURE().LoadTexture( "res/textures/building_option_border.dds", sampler);

	return true;
}

/**
* 
*
* @param :: 
*
* @return 
*/
void UserInterface::Shutdown() {
	// This is so the UserInterface's base class doesn't try to delete the texture an additional
	// time.
	this->mp_texture = nullptr;

	//for( s32 i( 0); i < gsc_countOptionsBuildings; ++i)
	//	MGR_TEXTURE().DeleteTexture( m_optionsBuildings[i].mp_texture);

	MGR_TEXTURE().DeleteTexture( mp_textureOptionBorder);

	SafeDeleteArray( m_optionsBuildings);
}

/**
* 
*
* @param :: 
*
* @return 
*/
bool UserInterface::OnUpdate( float deltaTime) {
	m_optionHovered = -1;

	if( mp_options == nullptr)
		return true;

	Vec2 pos;
	if( MGR_INPUT().GetMousePos( pos)) {
		if( pos.GetX() != -1 && pos.GetY() != -1) {
			if( MGR_INPUT().KeyPressed( 'P'))
				m_optionCount = m_optionCount;

			// Convert the cursor position to screen coordinates.
			pos.SetX( (pos.GetX() / SCREEN_WIDTH) - 0.5f);
			pos.SetY( ((pos.GetY() / SCREEN_HEIGHT) - 0.5f) * -1.0f);

			for( s32 i( 0); i < m_optionCount; ++i) {
				if( !this->CursorIsOverOption( &mp_options[i], pos))
					continue;

				m_optionHovered = i;
				break;
			}
		}
	}
	
	return UITexture::OnUpdate( deltaTime);
}

/**
* 
*
* @param :: 
*
* @return 
*/
void UserInterface::OnRender( float deltaTime) {
	if( mp_options == nullptr)
		return;

	// Render the options' icons.
	for( s32 i( 0); i < m_optionCount; ++i) {
		this->MoveTo( mp_options[i].m_pos);
		this->ScaleTo( mp_options[i].m_scale);

		
		this->mp_texture = Building::GetBuilding( i)->mp_texture;

		GET_APP()->m_matWorld = this->GetWorldMatrix();

		UITexture::OnRender( deltaTime);
	}

	// Render the border if an option is being hovered over.
	if( m_optionHovered != -1) {
		this->MoveTo( mp_options[m_optionHovered].m_pos);
		this->MoveBy( 0.0f, 0.0f, -0.1f);
		this->ScaleTo( mp_options[m_optionHovered].m_scale);

		this->mp_texture = mp_textureOptionBorder;

		GET_APP()->m_matWorld = this->GetWorldMatrix();

		UITexture::OnRender( deltaTime);
	}
}

/**
* 
*
* @param :: 
*
* @return 
*/
void UserInterface::ShowOptions( OptionList options) {
	m_optionHovered = -1;

	switch( options) {
		case OL_Buildings:
			mp_options = m_optionsBuildings;
			m_optionCount = Building::GetBuildingCount();
			break;
		case OL_None:
			mp_options = nullptr;
			m_optionCount = 0;
	}
}

/**
* 
*
* @param :: 
*
* @return 
*/
bool UserInterface::CursorIsOverOption( UIOption* option, const DBE::Vec2& mousePos) {
	float xMax( 0.0f), xMin( 0.0f);
	float yMax( 0.0f), yMin( 0.0f);
	float xMouse( mousePos.GetX()), yMouse( mousePos.GetY());

	xMax = (option->m_pos.GetX() + (option->m_scale.GetX() / 2));
	xMin = (option->m_pos.GetX() - (option->m_scale.GetX() / 2));
	yMax = (option->m_pos.GetY() + (option->m_scale.GetY() / 2));
	yMin = (option->m_pos.GetY() - (option->m_scale.GetY() / 2));

	if( xMouse >= xMin && xMouse <= xMax)
		if( yMouse >= yMin && yMouse <= yMax)
			return true;

	return false;
}