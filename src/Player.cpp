/********************************************************************
*	Function definitions for the Player class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "Player.h"

#include <DBEApp.h>
#include <DBEColour.h>
#include <DBECollision.h>
#include <DBEMath.h>
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;

static const s32 gsc_playerHealthMax( 100);
static const float gsc_flashCooldown( 2.0f);


/**
* Constructor.
*/
Player::Player()
	: BasicMesh( MeshType::MT_Sphere, 0.5f, 10.0f, 10.0f, WHITE)
	, m_health( gsc_playerHealthMax)
	, m_flashTimer( 0.0f)
{
	mp_testBall = new BasicMesh( MeshType::MT_Sphere, 0.25f, 10.0f, 10.0f, ORANGE);
}

/**
* Destructor.
*/
Player::~Player() {
	
}

/**
* Updates the player.
*
* @param deltaTime :: Time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool Player::Update( float deltaTime) {

	return BasicMesh::Update( deltaTime);
}

/**
* Renders the player.
*
* @param deltaTime :: Time taken to render the last frame.
*/
void Player::Render( float deltaTime) {
	BasicMesh::Render( deltaTime);
	mp_testBall->Render( deltaTime);
}

/**
* Gets the player's current health.
*
* @return The player's current health.
*/
s32 Player::GetHealth() {
	return m_health;
}

/**
* Heal the player for the specified amount.
*
* @param gainedHealth :: Amount of health for the player to gain.
*/
void Player::Heal( s32 gainedHealth) {
	m_health += gainedHealth;

	m_health = Clamp<s32>( m_health, 0, gsc_playerHealthMax);
}

/**
* Deal damage for the specified amount.
*
* @param lostHealth :: Amount of health for the player to lose.
*/
void Player::Injure( s32 lostHealth) {
	m_health -= lostHealth;

	m_health = Clamp<s32>( m_health, 0, gsc_playerHealthMax);
}

/**
* Returns whether or not the player has flash available.
*
* @return True if flash is available.
*/
bool Player::HasFlash() {
	return m_flashTimer == 0.0f;
}

/**
* Moves the player forward towards the cursor.
*
* @param mousePos	:: The mouse's position.
* @param p_floor	:: The bounds that the player must stay within.
*/
void Player::Flash( const DBE::Vec2& mousePos, const DBE::BasicMesh* p_floor) {
	float windowWidth( (float)GET_APP()->GetWindowWidth());
	float windowHeight( (float)GET_APP()->GetWindowHeight());
	XMFLOAT4X4 p;
	DirectX::XMStoreFloat4x4( &p, GET_APP()->m_matProj);

	// Compute picking ray in view space.
	float vx = ( 2.0f * mousePos.GetX() / windowWidth - 1.0f) / p._11;
	float vy = (-2.0f * mousePos.GetY() / windowHeight + 1.0f) / p._22;

	// Ray definition in view space.
	XMVECTOR rayOrigin	= XMVectorSet( 0.0f, 0.0f, 0.0f, 1.0f);
	XMVECTOR rayDir		= XMVectorSet( vx, vy, 1.0f, 0.0f);

	// Transform ray to local space of mesh.
	XMMATRIX v = GET_APP()->m_matView;
	XMMATRIX invView = XMMatrixInverse( &XMMatrixDeterminant( v), v);

	XMMATRIX w = XMMatrixIdentity();
	XMMATRIX invWorld = XMMatrixInverse( &XMMatrixDeterminant( w), w);

	XMMATRIX toLocal = XMMatrixMultiply( invView, invWorld);

	rayOrigin = XMVector3TransformCoord( rayOrigin, toLocal);
	rayDir = XMVector3TransformNormal( rayDir, toLocal);

	// Make the ray direction unit length for the intersection tests.
	rayDir = XMVector3Normalize( rayDir);

	// If we hit the bounding box of the mesh, then we might have picked a mesh triangle, so do the
	// ray/triangle tests.
	// If we did not hit the bounding box, then it is impossible that we hit the mesh, so do not
	// waste effort doing ray/triangle tests.

	// Find the nearest ray/triangle intersection.
	float tmin( DBE_Infinity);
	for( UINT i( 0); i < p_floor->m_indexCount / 3; ++i) {
		// Indicies for this triangle.
		UINT i0 = p_floor->mp_indicies[i*3+0];
		UINT i1 = p_floor->mp_indicies[i*3+1];
		UINT i2 = p_floor->mp_indicies[i*3+2];

		// Verticies for this triangle.
		Vec3 v0 = p_floor->mp_verts[i0].pos;
		Vec3 v1 = p_floor->mp_verts[i1].pos;
		Vec3 v2 = p_floor->mp_verts[i2].pos;

		// We have to iterate over all the triangles in order to find the nearest intersection.
		float t( 0.0f);
		if( Collision::IntersectRayTriangle( Vec3( rayOrigin), Vec3( rayDir), v0, v1, v2, &t))
			if( t < tmin)
				tmin = t;
	}

	Vec3 pickedPosition;
	pickedPosition = Vec3( rayOrigin) + (Vec3( rayDir) * tmin);
	pickedPosition = Vec3( pickedPosition.GetX(), 0.5f, pickedPosition.GetY());
	mp_testBall->MoveTo( pickedPosition);

	//if( m_picked == true) {
	//	m_dist = tmin;
	//	m_pickedPosition = Vec3( rayOrigin) + (Vec3( rayDir) * m_dist);
	//	m_pickedPosition = Vec3( m_pickedPosition.GetX(), 0.5f, m_pickedPosition.GetY());

	//	Vec3 velocity( m_pickedPosition - mp_player->GetPosition());
	//	velocity.Normalise();
	//	mp_player->SetVelocity( velocity * gsc_cubeSpeed);
	//}
}