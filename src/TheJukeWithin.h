/********************************************************************
*
*	CLASS		:: TheJukeWithin
*	DESCRIPTION	:: A small game where you have to j�ke!
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 06 / 03
*
********************************************************************/

#ifndef TheJukeWithinH
#define TheJukeWithinH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEApp.h>
#include <DBEBasicMesh.h>
#include <DBEColour.h>
#include <DBEMath.h>

#include "Enemy.h"
#include "Player.h"
//#include "Level.h"
//#include "UserInterface.h"

namespace DBE {
	class Camera;
	class Font;
}
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class TheJukeWithin : public DBE::App {
	public:
		/// Constructor.
		TheJukeWithin() {}

		/// Deals with initialisation.
		bool HandleInit();
		/// Deals with any input.
		bool HandleInput();
		/// Deals with any updating.
		bool HandleUpdate( float deltaTime);
		/// Deals with any rendering.
		bool HandleRender( float deltaTime);
		/// Deals with any 2D rendering.
		void HandleRender2D( float deltaTime);
		/// Deals with any 3D rendering.
		void HandleRender3D( float deltaTime);
		/// Deals with anything that needs to be released or shutdown.
		bool HandleShutdown();

	private:
		void Pick( const DBE::Vec2& mousePos);

		const char* m_windowTitle;

		DBE::Camera* mp_cam;

		DBE::Font*		mp_UIFont;
		//Level*			mp_level;
		//UserInterface*	mp_ui;

		DBE::BasicMesh* mp_floor;
		Player*			mp_player;
		Enemy*			mp_enemy;

		bool		m_picked;
		float		m_dist;
		DBE::Vec3	m_pickedPosition;

		float	m_camRadius;
		float	m_camPhi;
		float	m_camTheta;
		bool	m_rotate;

		bool m_wireframe;

		/// Private copy constructor to prevent multiple instances.
		TheJukeWithin( const TheJukeWithin&);
		/// Private assignment operator to prevent multiple instances.
		TheJukeWithin& operator=( const TheJukeWithin&);

};

APP_MAIN( TheJukeWithin, BLUE);

/*******************************************************************/
#endif	// #ifndef TheJukeWithinH