/********************************************************************
*
*	CLASS		:: Enemy
*	DESCRIPTION	:: An A.I. enemy that attempts to hit the player with skill shots.
*	CREATED BY	:: Edward Willoughby
*	DATE		:: 2015 / 06 / 03
*
********************************************************************/

#ifndef EnemyH
#define EnemyH

/********************************************************************
*	Include libraries and header files.
********************************************************************/
#include <DBEBasicMesh.h>
#include <DBERenderable.h>

class Player;
/********************************************************************
*	Defines and constants.
********************************************************************/


/*******************************************************************/
class Enemy : public DBE::BasicMesh {
	public:
		/// Constructor.
		Enemy( Player* p_player);
		/// Destructor.
		~Enemy();
		
		bool Reset();

		bool Update( float deltaTime);
		void Render( float deltaTime);
		
	private:
		enum EnemyState {
			ES_Waiting = 0,
			ES_Attacking,
		};

		void CheckPlayerHit();

		Player* mp_player;

		DBE::BasicMesh* mp_targetCircle;
		DBE::BasicMesh* mp_attackBall;

		EnemyState m_state;

		float m_timeUntilAttack;
		float m_attackTravelTime;

		DBE::Vec3 m_attackOrigin;
		
		/// Private copy constructor to prevent accidental multiple instances.
		Enemy( const Enemy& other);
		/// Private assignment operator to prevent accidental multiple instances.
		Enemy& operator=( const Enemy& other);
		
};

/*******************************************************************/
#endif	// #ifndef EnemyH
