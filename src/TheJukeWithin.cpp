/********************************************************************
*	Function definitions for the TheJukeWithin class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "TheJukeWithin.h"

#include <DBECamera.h>
#include <DBECollision.h>
#include <DBEColour.h>
#include <DBEFont.h>

/********************************************************************
*	Defines, constants, namespaces, and local variables.
********************************************************************/
using namespace DBE;
using namespace DirectX;

static u32 gs_frames = 0;
static float gs_deltaTotal = 0.0f;

static DBE::BasicMesh* gsp_XZPlane( nullptr);

static const float gsc_cubeSpeed( 0.12f);


/**
* Deals with initialisation.
*
* @return True if everything initialised correctly.
*/
bool TheJukeWithin::HandleInit() {
	m_camRadius	= 10.0f;
	m_camTheta	= DBE_Pi * 1.5f;
	m_camPhi	= DBE_Pi * 0.25f;
	m_rotate	= false;

	mp_cam = new Camera();
	//mp_cam->m_position = Vec3( 0.0f, 10.0f, -10.0f);
	mp_cam->m_position = Vec3( 0.0f, 18.0f, -12.0f);
	mp_cam->m_lookAt = Vec3( 0.0f, 0.0f, -3.0f);

	//mp_level = new Level();
	//if( !mp_level->LoadLevel( 0))
	//	return false;

	//mp_ui = new UserInterface();
	//if( !mp_ui->Init())
	//	return false;

	// Create the 'invisible floor'.
	float size( 5000.0f);
	gsp_XZPlane = new BasicMesh( MeshType::MT_Plane, size, size, size, WHITE);
	if( gsp_XZPlane == nullptr)
		return false;
	gsp_XZPlane->RotateBy( DBE_ToRadians( 90.0f), 0.0f, 0.0f);

	Font::InstallFont( "res/fonts/TimeGoesBySoSlowly.ttf");
	mp_UIFont = Font::CreateByName( "FontTimeGoesBySoSlowly", "Time goes by so slowly", 20, 0);

	// Create the floor.
	mp_floor = new BasicMesh( MeshType::MT_Plane, 10.0f, 10.0f, 0.0f, GREEN);
	mp_floor->RotateBy( DBE_ToRadians( 90.0f), 0.0f, 0.0f);

	// Create the player.
	mp_player = new Player();

	// Create the (an) test enemy.
	mp_enemy = new Enemy( mp_player);
	mp_enemy->MoveTo( 7.0f, 0.5f, 0.0f);

	m_picked = false;
	m_pickedPosition = Vec3( 0.0f);
	m_dist = 0.0f;

	this->EnableLightDirectional( 0, Vec3( -1.0f, -1.0f, -1.0f), Vec3( 1.0f, 1.0f, 1.0f)); //0.75f, 0.75f, 0.85f));

	m_wireframe = false;

	m_windowTitle = "The J�ke Within";
	this->SetWindowTitle( m_windowTitle);

	this->SetFocusResponse( FocusResponse::FR_SlowRendering);

	return true;
}

/**
* Deals with any input.
*
* @return True if nothing went wrong.
*/
bool TheJukeWithin::HandleInput() {
	// Toggle wireframe mode.
	//if( MGR_INPUT().KeyPressed( 'W'))
	//	m_wireframe = !m_wireframe;

	// Quit the program.
	if( MGR_INPUT().KeyPressed( VK_ESCAPE))
		this->Terminate();

	if( MGR_INPUT().KeyPressed( VK_LBUTTON) || MGR_INPUT().KeyHeld( VK_LBUTTON)) {
		Vec2 mousePos;
		MGR_INPUT().GetMousePos( mousePos);
		this->Pick( mousePos);
	}

	if( MGR_INPUT().KeyPressed( 'F')) {
		if( mp_player->HasFlash()) {
			Vec2 mousePos;
			MGR_INPUT().GetMousePos( mousePos);
			mp_player->Flash( mousePos, gsp_XZPlane);
		}
	}

	static float mov( 0.1f);
	if( MGR_INPUT().KeyHeld( 'W'))
		mp_player->MoveBy( mov, 0.0f, 0.0f);
	if( MGR_INPUT().KeyHeld( 'S'))
		mp_player->MoveBy( -mov, 0.0f, 0.0f);
	if( MGR_INPUT().KeyHeld( 'A'))
		mp_player->MoveBy( 0.0f, 0.0f, mov);
	if( MGR_INPUT().KeyHeld( 'D'))
		mp_player->MoveBy( 0.0f, 0.0f, -mov);

	return true;
}

/**
* Deals with any updating.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool TheJukeWithin::HandleUpdate( float deltaTime) {
	// Set the window title.
	char buffer[256];
	sprintf_s( buffer, sizeof( buffer), "%s - FPS: %f, Delta: %f", m_windowTitle, FRAME_TIMER().FPS(), FRAME_TIMER().DeltaTime());
	this->SetWindowTitle( buffer);

	mp_enemy->Update( deltaTime);

	if( m_picked) {
		// If the distance remaining is larger than the cube's velocity then just move to the
		// position, otherwise move based on the velocity.
		Vec3 velocity( mp_player->GetVelocity());
		Vec3 distance( m_pickedPosition - mp_player->GetPosition());
		distance.Abs();
		velocity.Abs();
		if( distance.GetX() <= velocity.GetX() &&
			distance.GetY() <= velocity.GetY() &&
			distance.GetZ() <= velocity.GetZ())
		{
			mp_player->MoveTo( m_pickedPosition);
			m_picked = false;
		}
		else {
			mp_player->MoveBy( mp_player->GetVelocity());
		}
	}
	mp_player->RotateBy( 0.0f, DBE_ToRadians( 1.0f), 0.0f);
	mp_player->Update( deltaTime);

	mp_floor->Update( deltaTime);

	//mp_level->Update( deltaTime);

	//mp_ui->Update( deltaTime);

	mp_cam->Update();

	++gs_frames;
	gs_deltaTotal += deltaTime;

	return true;
}

/**
* Deals with any rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*
* @return True if everything rendered correctly.
*/
bool TheJukeWithin::HandleRender( float deltaTime) {
	this->HandleRender3D( deltaTime);
	this->HandleRender2D( deltaTime);

	// Re-apply the 3D projection matrix and view matrix so they're stored for the next update and
	// can thus be used for 'picking'.
	mp_cam->ApplyPerspectiveMatrixLH( 1.0f, 1000.0f);
	mp_cam->ApplyViewMatrixLH();

	return true;
}

/**
* Deals with any 2D rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*/
void TheJukeWithin::HandleRender2D( float deltaTime) {
	mp_cam->ApplyOrthoMatrixLH( 1.0f, 1000.0f);

	DEFAULT_FONT()->DrawStringf2D( Vec3( -0.49f, 0.45f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Health: %i", mp_player->GetHealth());

	//mp_UIFont->DrawStringf2D( Vec3( 0.40f, 0.45f, 0.0f), Vec3( 0.0f), 1.0f, &Font::Style( GOLD), "%ig", mp_level->GetGold());

	//mp_ui->Render( deltaTime);

	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.275f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Picked: %i", mp_level->GetHoveredTile());
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.250f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Highlight: %f", mp_level->GetHoveredTileHightlight());
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.225f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "Building Hovered: %i", mp_ui->GetSelectedOption());
	
	//Vec2 pos, screen;
	//if( MGR_INPUT().GetMousePos( pos)) {
	//	if( pos.GetX() != -1 && pos.GetY() != -1) {
	//		screen.SetX( (pos.GetX() / SCREEN_WIDTH) - 0.5f);
	//		screen.SetY( ((pos.GetY() / SCREEN_HEIGHT) - 0.5f) * -1.0f);
	//	}
	//}
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.125f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "X: %f, Y: %f\n", pos.GetX(), pos.GetY());
	//DEFAULT_FONT()->DrawStringf2D( Vec3( -0.5f, 0.100f, 0.0f), Vec3( 0.0f), 1.0f, nullptr, "X: %f, Y: %f\n", screen.GetX(), screen.GetY());
}

/**
* Deals with any 3D rendering.
*
* @param deltaTime :: The time taken to render the last frame.
*/
void TheJukeWithin::HandleRender3D( float deltaTime) {
	mp_cam->ApplyPerspectiveMatrixLH( 1.0f, 1000.0f);
	mp_cam->ApplyViewMatrixLH();

	this->SetBlendState( true);
	this->SetDepthStencilState( true, true);
	this->SetRasteriserState( false, m_wireframe);

	mp_floor->Render( deltaTime);
	mp_player->Render( deltaTime);
	mp_enemy->Render( deltaTime);

	//mp_level->Render( deltaTime);
}

/**
* Deals with anything that needs to be released or shutdown.
*
* @return True if everything was closed correctly.
*/
bool TheJukeWithin::HandleShutdown() {
	//mp_ui->Shutdown();

	SafeDelete( gsp_XZPlane);

	SafeDelete( mp_floor);
	SafeDelete( mp_player);
	SafeDelete( mp_enemy);

	//SafeDelete( mp_level);
	//SafeDelete( mp_ui);

	DebugTrace( "Frames: %i\nDelta Average: %f\nAverage F.P.S.: %f\n", gs_frames, gs_deltaTotal / gs_frames, gs_frames / gs_deltaTotal);

	return true;
}

void TheJukeWithin::Pick( const DBE::Vec2& mousePos) {
	float windowWidth( (float)GET_APP()->GetWindowWidth());
	float windowHeight( (float)GET_APP()->GetWindowHeight());
	XMFLOAT4X4 p;
	DirectX::XMStoreFloat4x4( &p, GET_APP()->m_matProj);

	// Compute picking ray in view space.
	float vx = ( 2.0f * mousePos.GetX() / windowWidth - 1.0f) / p._11;
	float vy = (-2.0f * mousePos.GetY() / windowHeight + 1.0f) / p._22;

	// Ray definition in view space.
	XMVECTOR rayOrigin	= XMVectorSet( 0.0f, 0.0f, 0.0f, 1.0f);
	XMVECTOR rayDir		= XMVectorSet( vx, vy, 1.0f, 0.0f);

	// Transform ray to local space of mesh.
	XMMATRIX v = GET_APP()->m_matView;
	XMMATRIX invView = XMMatrixInverse( &XMMatrixDeterminant( v), v);

	XMMATRIX w = mp_floor->GetWorldMatrix();
	XMMATRIX invWorld = XMMatrixInverse( &XMMatrixDeterminant( w), w);

	XMMATRIX toLocal = XMMatrixMultiply( invView, invWorld);

	rayOrigin = XMVector3TransformCoord( rayOrigin, toLocal);
	rayDir = XMVector3TransformNormal( rayDir, toLocal);

	// Make the ray direction unit length for the intersection tests.
	rayDir = XMVector3Normalize( rayDir);

	// If we hit the bounding box of the mesh, then we might have picked a mesh triangle, so do the
	// ray/triangle tests.
	// If we did not hit the bounding box, then it is impossible that we hit the mesh, so do not
	// waste effort doing ray/triangle tests.

	m_picked = false;
	float tmin = 0.0f;

	// Find the nearest ray/triangle intersection.
	tmin = DBE_Infinity;
	for( UINT i( 0); i < mp_floor->m_indexCount / 3; ++i) {
		// Indicies for this triangle.
		UINT i0 = mp_floor->mp_indicies[i*3+0];
		UINT i1 = mp_floor->mp_indicies[i*3+1];
		UINT i2 = mp_floor->mp_indicies[i*3+2];

		// Verticies for this triangle.
		Vec3 v0 = mp_floor->mp_verts[i0].pos;
		Vec3 v1 = mp_floor->mp_verts[i1].pos;
		Vec3 v2 = mp_floor->mp_verts[i2].pos;

		// We have to iterate over all the triangles in order to find the nearest intersection.
		float t( 0.0f);
		if( Collision::IntersectRayTriangle( Vec3( rayOrigin), Vec3( rayDir), v0, v1, v2, &t)) {
			if( t < tmin) {
				// This is the new nearest picked triangle.
				tmin = t;
				m_picked = true;
			}
		}

		DebugTrace( "Distance: %f\n", t);
	}

	if( m_picked == true) {
		m_dist = tmin;
		m_pickedPosition = Vec3( rayOrigin) + (Vec3( rayDir) * m_dist);
		m_pickedPosition = Vec3( m_pickedPosition.GetX(), 0.5f, m_pickedPosition.GetY());

		Vec3 velocity( m_pickedPosition - mp_player->GetPosition());
		velocity.Normalise();
		mp_player->SetVelocity( velocity * gsc_cubeSpeed);
	}
}