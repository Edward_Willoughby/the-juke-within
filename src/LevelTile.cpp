/********************************************************************
*	Function definitions for the LevelTile class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "LevelTile.h"

/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;


/**
* Constructor.
*/
LevelTile::LevelTile()
	: DBE::BasicMesh( MeshType::MT_Plane)
{
	
}

/**
* Destructor.
*/
LevelTile::~LevelTile() {
	
}

/**
* 
*
* @param :: 
*
* @return 
*/
