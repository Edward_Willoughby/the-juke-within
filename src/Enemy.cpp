/********************************************************************
*	Function definitions for the Enemy class.
********************************************************************/

/********************************************************************
*	Include the header file.
********************************************************************/
#include "Enemy.h"

#include <DBEColour.h>
#include <DBERandom.h>

#include "Player.h"
/********************************************************************
*	Namespaces, defines, constants, and local variables.
********************************************************************/
using namespace DBE;

static const float gsc_attackTravelTime( 0.5f);
static const float gsc_attackCooldown( 2.0f);

static const float gsc_attackDelayMin( 0.0f);
static const float gsc_attackDelayMax( 3.0f);


/**
* Constructor.
*/
Enemy::Enemy( Player* p_player)
	: BasicMesh( MeshType::MT_Cone, 1.0f, 0.0f, 1.0f, WHITE)
	, mp_player( p_player)
	, mp_targetCircle( nullptr)
	, m_state( EnemyState::ES_Waiting)
	, m_timeUntilAttack( 0.0f)
	, m_attackTravelTime( 0.0f)
{
	mp_targetCircle = new BasicMesh( MeshType::MT_Cylinder, 1.0f, 0.001f, 0.0f, WHITE);
	mp_attackBall = new BasicMesh( MeshType::MT_Sphere, 0.25f, 20, 20, RED);

	m_timeUntilAttack = gsc_attackCooldown;
}

/**
* Destructor.
*/
Enemy::~Enemy() {
	SafeDelete( mp_targetCircle);
	SafeDelete( mp_attackBall);
}

/**
* Resets the enemy.
*
* @return True if everything was reset correctly.
*/
bool Enemy::Reset() {
	m_attackTravelTime = 0.0f;
	m_timeUntilAttack = 0.0f;

	return true;
}

/**
* Updates the enemy.
*
* @param deltaTime :: Time taken to render the last frame.
*
* @return True if everything updated correctly.
*/
bool Enemy::Update( float deltaTime) {
	//this->RotateBy( 0.0f, 0.1f, 0.0f);

	switch( m_state) {
		case EnemyState::ES_Waiting:
			m_timeUntilAttack -= deltaTime;

			if( m_timeUntilAttack < 0.0f) {
				m_attackTravelTime = gsc_attackTravelTime;
				m_state = EnemyState::ES_Attacking;

				// Set the attack's origin.
				m_attackOrigin = this->GetPosition();
				mp_attackBall->MoveTo( this->GetPosition());

				// Set the attack's target.
				Vec3 pos( mp_player->GetPosition());
				Vec3 vel( mp_player->GetVelocity());
				if( vel.IsZero()) {
					mp_targetCircle->MoveTo( pos);
				}
				else {
					float moe( Random::Float( -1.0f, 2.0f));
					Vec3 offset( vel * moe);

					mp_targetCircle->MoveTo( pos + offset);
				}

				pos = mp_targetCircle->GetPosition();
				pos.SetY( 0.0f);
				mp_targetCircle->MoveTo( pos);
			}
			break;

		case EnemyState::ES_Attacking:
			m_attackTravelTime -= deltaTime;

			if( m_attackTravelTime <= 0.0f) {
				m_attackTravelTime = 0.0f;
				m_state = EnemyState::ES_Waiting;

				m_timeUntilAttack = gsc_attackCooldown + Random::Float( gsc_attackDelayMin, gsc_attackDelayMax);

				mp_attackBall->MoveTo( mp_targetCircle->GetPosition());

				CheckPlayerHit();
			}
			else {
				// Should be a value between 0.0f and 1.0f; 1.0f means it's just started while 0.0f
				// means that it's at its destination.
				float interp( m_attackTravelTime / gsc_attackTravelTime);

				Vec3 newPos( mp_targetCircle->GetPosition() - m_attackOrigin);
				newPos = newPos - (newPos * interp);

				mp_attackBall->MoveTo( m_attackOrigin + newPos);
			}

			break;
	}

	return BasicMesh::Update( deltaTime);
}

/**
* Renders the enemy and any spell effects they're responsible for.
*
* @param deltaTime :: Time taken to render the last frame.
*/
void Enemy::Render( float deltaTime) {
	BasicMesh::Render( deltaTime);

	// Render the 'attack' if the enemy is attacking.
	if( m_state == EnemyState::ES_Attacking) {
		mp_targetCircle->Render( deltaTime);
		mp_attackBall->Render( deltaTime);
	}
}

/**
* Tests if the player was hit by the attack.
*/
void Enemy::CheckPlayerHit() {
	float dist( Distance( mp_player->GetPosition(), mp_targetCircle->GetPosition()));

	// Hard-coded for now; the target's radius is 1.0f and the player's is 0.5f.
	float maxDist( 1.0f + 0.5f);
	
	// Early-out if the player is farther away from the attack than their combined radii.
	if( dist >= maxDist)
		return;

	// If it's close enough, hit the player for damage.
	mp_player->Injure( 25);
}